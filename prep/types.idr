
-- Enums
data Class = Economy | Business | First

upgrade : Class -> Class
upgrade Economy = Business
upgrade Business = First
upgrade First = First

-- Union
data Destination = Coordinates Double Double
                 | City String

showDestination : Destination -> String
showDestination (Coordinates x y) = show x ++ ", " ++ show y
showDestination (City str) = str

-- Recursive
-- data Flight = OneWay Destination Destination Class
--            | RoundTrip Flight Flight

-- data Flight : Type where
--   OneWay : Destination -> Destination -> Class -> Route
--   RoundTrip : Flight -> Flight -> Flight

-- dependent
data Flight : Class -> Type where
  OneWay : Destination -> Destination -> (c : Class) -> Flight c
  RoundTrip : Flight c -> Flight c -> Flight c

flight1 : Flight Economy
flight1 = OneWay (City "Toronto") (City "New York") _

flight2 : Flight Business
flight2 = OneWay (City "New York") (City "Toronto") _

upgradeFlight : Flight c -> Flight (upgrade c)
upgradeFlight (OneWay x y c) = OneWay x y (upgrade c)
upgradeFlight (RoundTrip x y) = RoundTrip (upgradeFlight x) (upgradeFlight y)


