
reverse_string : String -> String
reverse_string str = let chars = unpack str in
  (pack (reverse_chars chars)) where
    reverse_chars : List Char -> List Char
    reverse_chars [] = []
    reverse_chars (x :: xs) = (reverse_chars xs) ++ [x]

