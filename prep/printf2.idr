
-- printf "%s is %d" "foobar" 42


data Format = Lit Char Format
            | Decimal Format
            | Str Format
            | End

toFormat : String -> Format
toFormat str = toFormat' (unpack str) where
  toFormat' : List Char -> Format
  toFormat' [] = End
  toFormat' ('%' :: 'd' :: xs) = Decimal (toFormat' xs)
  toFormat' ('%' :: 's' :: xs) = Str (toFormat' xs)
  toFormat' (x :: xs) = Lit x (toFormat' xs)


printfType : Format -> Type
printfType (Lit x fmt) = printfType fmt
printfType (Decimal fmt) = Int -> printfType fmt
printfType (Str fmt) = String -> printfType fmt
printfType End = String

printf : (str : String) -> printfType (toFormat str)
printf str = printf' _ "" where
  printf' : (fmt : Format) -> (out : String) -> printfType fmt
  printf' (Lit c fmt') out = printf' fmt' (out ++ cast c)
  printf' (Decimal fmt') out = \i => printf' fmt' (out ++ show i)
  printf' (Str fmt') out = \str' => printf' fmt' (out ++ str')
  printf' End out = out
