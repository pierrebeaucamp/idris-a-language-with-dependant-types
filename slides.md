---
title: Idris - a language with dependent types
author: pierrebeaucamp
...

	     ____    __     _
	    /  _/___/ /____(_)____
	    / // __  / ___/ / ___/
	  _/ // /_/ / /  / (__  )      http://www.idris-lang.org/
	 /___/\__,_/_/  /_/____

---

	     ____    __     _
	    /  _/___/ /____(_)____
	    / // __  / ___/ / ___/
	  _/ // /_/ / /  / (__  )      http://www.idris-lang.org/
	 /___/\__,_/_/  /_/____

- Author: Edwin Brady
- General purpose
- Purely functional
- Dependent types

---

	     ____    __     _
	    /  _/___/ /____(_)____
	    / // __  / ___/ / ___/
	  _/ // /_/ / /  / (__  )      http://www.idris-lang.org/
	 /___/\__,_/_/  /_/____

- Eager evaluation
- Totality checking
- Interactive development environment
- C & JS Compile targets

---

# Type driven development (TDD)

- Write input & output types
- Define a function
- Refine original types
